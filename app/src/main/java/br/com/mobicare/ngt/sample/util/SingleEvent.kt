package br.com.mobicare.ngt.sample.util

class SingleEvent<out T>(private val _content: T?) {
    var wasHandled: Boolean = _content == null
        private set

    val consume: T?
        get() = if (wasHandled) {
            null
        } else {
            wasHandled = true
            _content
        }

    val peek: T?
        get() = _content
}