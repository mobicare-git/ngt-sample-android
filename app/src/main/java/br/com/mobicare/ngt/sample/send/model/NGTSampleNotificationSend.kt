package br.com.mobicare.ngt.sample.send.model

import com.google.gson.JsonElement
import com.google.gson.JsonNull
import com.google.gson.JsonSerializationContext
import com.google.gson.JsonSerializer
import com.google.gson.annotations.JsonAdapter
import com.google.gson.annotations.SerializedName
import java.lang.reflect.Type

data class NGTSampleNotificationSend(
    @SerializedName("userKey")
    val userKey: String,
    @SerializedName("message")
    val message: String,
    @SerializedName("campaign")
    val campaign: String = "2022",
    @SerializedName("parameters")
    @JsonAdapter(ParamsAdapter::class)
    val params: MutableMap<String, String> = mutableMapOf()
) {

    class ParamsAdapter : JsonSerializer<Map<String, String>> {
        override fun serialize(
            src: Map<String, String>?,
            typeOfSrc: Type?,
            context: JsonSerializationContext
        ): JsonElement {
            if (src == null || src.isEmpty()) {
                return JsonNull.INSTANCE
            }
            val items = src.entries.map {
                mapOf(
                    Pair("key", it.key),
                    Pair("value", it.value),
                    Pair("send", true)
                )
            }
            return context.serialize(items)
        }
    }

}
