package br.com.mobicare.ngt.sample.util

import android.app.Activity
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment

fun Activity.closeKeyboard() {
    val focus = currentFocus ?: return
    ContextCompat.getSystemService(this, InputMethodManager::class.java)
        ?.hideSoftInputFromWindow(focus.windowToken, 0)
}

fun Fragment.closeKeyboard() {
    activity?.closeKeyboard()
}