package br.com.mobicare.ngt.sample.send.model

import com.google.gson.annotations.SerializedName

data class NGTSampleOauthResponse(
    @SerializedName("token_type")
    val type: String,
    @SerializedName("access_token")
    val token: String,
)
