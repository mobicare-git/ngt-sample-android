package br.com.mobicare.ngt.sample.ui.home

import android.app.Application
import android.net.Uri
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import br.com.mobicare.ngt.NGT
import br.com.mobicare.ngt.sample.R
import br.com.mobicare.ngt.sample.notification.SampleNotificationUserKey
import br.com.mobicare.ngt.sample.send.api.NGTSamplePushSendApi
import br.com.mobicare.ngt.sample.send.model.NGTSampleNotificationSend
import br.com.mobicare.ngt.sample.util.SingleEvent
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val _userKey = MutableLiveData(SampleNotificationUserKey.getUserKey(application))
    private val _registered = MutableLiveData(false)
    private val _loading = MutableLiveData(false)
    private val _error = MutableLiveData(SingleEvent<Throwable>(null))
    private val _successSnack = MutableLiveData(SingleEvent<Int>(null))
    private val _curl = MutableLiveData("")
    private val ngt: NGT by lazy { NGT.getInstance(application) }

    val userKey: LiveData<String>
        get() = _userKey

    val registered: LiveData<Boolean>
        get() = _registered

    val loading: LiveData<Boolean>
        get() = _loading

    val error: LiveData<SingleEvent<Throwable>>
        get() = _error

    val successSnack: LiveData<SingleEvent<Int>>
        get() = _successSnack

    val curl: LiveData<String>
        get() = _curl

    fun register() {
        _loading.value = true
        FirebaseMessaging.getInstance().token.onSuccessTask {
            ngt.register(it, SampleNotificationUserKey.getUserKey(getApplication()))
        }.addOnSuccessListener {
            _loading.value = false
            _registered.value = true
            _successSnack.value = SingleEvent(R.string.home_success_register)
        }.addOnFailureListener {
            _loading.value = false
            _registered.value = false
            _error.value = SingleEvent(it)
        }
    }

    fun unregister() {
        _loading.value = true
        ngt.unregister().addOnSuccessListener {
            _loading.value = false
            _registered.value = false
            _successSnack.value = SingleEvent(R.string.home_success_unregister)
        }.addOnFailureListener {
            _loading.value = false
            _registered.value = true
            _error.value = SingleEvent(it)
        }
    }

    fun sendPush(title: String?, message: String?, image: String?) {
        _loading.value = true
        _curl.value = ""
        val notification = NGTSampleNotificationSend(
            SampleNotificationUserKey.getUserKey(getApplication()),
            message ?: ""
        )
        if (!title.isNullOrBlank()) {
            notification.params["title"] = title
        }
        if (!image.isNullOrBlank()) {
            val uri = try {
                Uri.parse(image)
            } catch (e: Exception) {
                _loading.value = false
                _error.value = SingleEvent(e)
                return
            }
            if (uri.scheme?.startsWith("http") != true) {
                _loading.value = false
                _error.value =
                    SingleEvent(IllegalArgumentException("Image URL must be http or https"))
                return
            }
            notification.params["banner-url"] = image
        }
        NGTSamplePushSendApi(Gson()).sendPushNotificationCaptureCurl(
            notification
        ).addOnSuccessListener {
            _loading.value = false
            _curl.value = it
            _successSnack.value = SingleEvent(R.string.notification_success_send)
        }.addOnFailureListener {
            _loading.value = false
            _error.value = SingleEvent(it)
        }

    }

}