package br.com.mobicare.ngt.sample.ui.curl

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.view.ViewGroup
import android.view.Window
import androidx.appcompat.app.AppCompatDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import br.com.mobicare.ngt.sample.R
import br.com.mobicare.ngt.sample.databinding.DialogCurlBinding

class CurlDialog(context: Context) : AppCompatDialog(context, R.style.Style_LoadingDialog) {

    private var binding: DialogCurlBinding

    init {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_curl)
        binding = DialogCurlBinding.bind(findViewById(R.id.dialog_curl_root)!!)

        binding.dialogCurlClose.setOnClickListener {
            dismiss()
        }
        binding.dialogCurlCopy.setOnClickListener {
            copyContent()
        }
    }

    fun setCurl(content: String) {
        binding.dialogCurlContent.text = content
    }

    override fun show() {
        super.show()
        window?.setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
    }

    private fun copyContent() {
        val manager = ContextCompat.getSystemService(context, ClipboardManager::class.java)
        binding.dialogCurlCopied.isVisible = true
        if (manager == null) {
            binding.dialogCurlCopied.setText(R.string.dialog_curl_copy_failed)
            return
        }
        binding.dialogCurlCopied.setText(R.string.dialog_curl_copy_success)
        manager.setPrimaryClip(ClipData.newPlainText("cURL", binding.dialogCurlContent.text))
    }

}