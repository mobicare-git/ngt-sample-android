package br.com.mobicare.ngt.sample.notification

import android.app.NotificationManager
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import br.com.mobicare.ngt.NGT
import br.com.mobicare.ngt.sample.R
import com.bumptech.glide.Glide
import com.bumptech.glide.load.model.GlideUrl
import com.bumptech.glide.load.model.LazyHeaders
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.transition.Transition
import com.google.android.gms.tasks.Tasks
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class SampleNotificationService : FirebaseMessagingService() {
    override fun onNewToken(token: String) {
        super.onNewToken(token)
        Tasks.await(NGT.getInstance(this).updateFcmToken(token))
    }

    override fun onMessageReceived(_message: RemoteMessage) {
        Log.d("Sample/" + javaClass.simpleName, "Received FCM message")
        val ngt = NGT.getInstance(this).getNotification(_message.data)
        if (ngt == null) {
            Log.w("Sample/" + javaClass.simpleName, "Received message is not NGT notification")
            return
        }

        val title = ngt.title
        val message = ngt.message
        val channel = SampleNotificationChannel.PUSH

        val builder = NotificationCompat.Builder(this, channel.id)
            .setSmallIcon(R.drawable.ic_notification_channel_default)
            .setAutoCancel(true)
            .setContentTitle(title ?: getString(R.string.app_name))
            .setContentText(message)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setDeleteIntent(ngt.createDeletePendingIntent(this))
            .setContentIntent(ngt.createContentPendingIntent(this))

        if (!ngt.data["banner-url"].isNullOrBlank()) {
            val url = GlideUrl(
                ngt.data["banner-url"],
                LazyHeaders.Builder().addHeader("User-Agent", "Glide/4.13.0").build()
            )
            Glide.with(this)
                .asBitmap()
                .load(url)
                .into(object : CustomTarget<Bitmap>() {
                    override fun onResourceReady(
                        resource: Bitmap,
                        transition: Transition<in Bitmap>?
                    ) {
                        builder.setStyle(
                            NotificationCompat.BigPictureStyle().bigPicture(resource)
                                .setSummaryText(message)
                        )
                        showNotification(channel, builder)
                    }

                    override fun onLoadFailed(errorDrawable: Drawable?) {
                        builder.setStyle(NotificationCompat.BigTextStyle().bigText(message))
                        showNotification(channel, builder)
                    }

                    override fun onLoadCleared(placeholder: Drawable?) {
                    }
                })
            return
        } else {
            builder.setStyle(NotificationCompat.BigTextStyle().bigText(message))
        }

        showNotification(channel, builder)
    }

    private fun showNotification(
        channel: SampleNotificationChannel,
        builder: NotificationCompat.Builder
    ) {
        val manager = ContextCompat.getSystemService(this, NotificationManager::class.java)!!
        channel.create(this, manager)
        val notification = builder.build()
        manager.notify(notification.hashCode(), notification)
    }
}