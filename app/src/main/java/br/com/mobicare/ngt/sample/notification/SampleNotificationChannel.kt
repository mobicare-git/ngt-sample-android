package br.com.mobicare.ngt.sample.notification

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import androidx.annotation.StringRes
import br.com.mobicare.ngt.sample.R

@Suppress("MemberVisibilityCanBePrivate")
@SuppressLint("InlinedApi")
enum class SampleNotificationChannel(
    val id: String,
    @StringRes val title: Int,
    @StringRes val description: Int,
    val importance: Int
) {
    DEFAULT(
        "default",
        R.string.notification_channel_default_title,
        R.string.notification_channel_default_description,
        NotificationManager.IMPORTANCE_DEFAULT
    ),
    PUSH(
        "push",
        R.string.notification_channel_push_title,
        R.string.notification_channel_push_description,
        NotificationManager.IMPORTANCE_HIGH
    ),

    ;

    fun create(context: Context, manager: NotificationManager) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O)
            return
        if (manager.getNotificationChannel(id) != null)
            return
        val channel = NotificationChannel(id, context.getString(title), importance)
        channel.description = context.getString(description)
        manager.createNotificationChannel(channel)
    }

}