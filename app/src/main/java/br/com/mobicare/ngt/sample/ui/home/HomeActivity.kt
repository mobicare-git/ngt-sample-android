package br.com.mobicare.ngt.sample.ui.home

import android.content.ClipData
import android.content.ClipboardManager
import android.os.Bundle
import android.util.Log
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProvider
import br.com.mobicare.ngt.NGT
import br.com.mobicare.ngt.sample.R
import br.com.mobicare.ngt.sample.databinding.ActivityHomeBinding
import br.com.mobicare.ngt.sample.databinding.ContentHomeBinding
import br.com.mobicare.ngt.sample.ui.curl.CurlDialog
import br.com.mobicare.ngt.sample.ui.loading.LoadingDialog
import com.google.android.material.snackbar.Snackbar

class HomeActivity : AppCompatActivity() {
    private val vm by lazy { ViewModelProvider(this)[HomeViewModel::class.java] }
    private lateinit var binding: ActivityHomeBinding
    private lateinit var content: ContentHomeBinding
    private val loading by lazy { LoadingDialog(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        content = ContentHomeBinding.bind(binding.homeContentRoot)
        setContentView(binding.root)

        val version = packageManager.getPackageInfo(packageName, 0).versionName
        val sdk = NGT.getInstance(this).getVersion()
        binding.homeAppBarVersion.text = getString(R.string.splash_version, sdk, version)

        vm.userKey.observe(this) { userKey ->
            content.homeUserKey.text = getString(R.string.home_user_key, userKey)
            content.homeUserKeyCopy.setOnClickListener {
                val manager = ContextCompat.getSystemService(this, ClipboardManager::class.java)
                if (manager == null) {
                    showSnackBar(R.string.home_copy_user_key_failure)
                    return@setOnClickListener
                }
                manager.setPrimaryClip(
                    ClipData.newPlainText(
                        getString(R.string.home_copy_user_key_label),
                        userKey
                    )
                )
                showSnackBar(R.string.home_copy_user_key_success)
            }
        }
        vm.registered.observe(this) {
            content.homeRegister.isEnabled = !it
            content.homeUnregister.isEnabled = it
        }
        vm.loading.observe(this) {
            if (it) {
                showLoading()
            } else {
                hideLoading()
            }
        }
        vm.error.observe(this) { event ->
            event.consume?.let {
                showError(it)
            }
        }
        vm.successSnack.observe(this) { event ->
            event.consume?.let {
                showSnackBar(it)
            }
        }
        vm.curl.observe(this) { curl ->
            if (curl.isEmpty()) {
                content.homeSeeCurl.isVisible = false
                content.homeSeeCurl.setOnClickListener(null)
            } else {
                content.homeSeeCurl.isVisible = true
                content.homeSeeCurl.setOnClickListener {
                    showCurlContent(curl)
                }
            }
        }

        content.homeRegister.setOnClickListener {
            vm.register()
        }
        content.homeUnregister.setOnClickListener {
            vm.unregister()
        }
        content.homeContentImageCheck.setOnCheckedChangeListener { _, checked ->
            content.homeContentImageBox.isVisible = checked
        }
        content.homeSendPush.setOnClickListener {
            vm.sendPush(
                content.homeContentTitle.text?.toString(),
                content.homeContentMessage.text?.toString(),
                if (content.homeContentImageCheck.isChecked) content.homeContentImage.text?.toString() else null
            )
        }

        content.homeContentImage.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus && content.homeContentImage.text.isNullOrBlank()) {
                content.homeContentImage.setText(R.string.push_notification_sample_image)
            }
        }
        content.homeContentMessage.setOnFocusChangeListener { _, hasFocus ->
            if (!hasFocus && content.homeContentMessage.text.isNullOrBlank()) {
                content.homeContentMessage.setText(R.string.push_notification_sample_message)
            }
        }
    }

    private fun showLoading() {
        if (loading.isShowing)
            return
        loading.show()
    }

    private fun hideLoading() {
        if (!loading.isShowing)
            return
        loading.dismiss()
    }

    private fun showSnackBar(message: CharSequence) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_SHORT).show()
    }

    private fun showSnackBar(@StringRes id: Int) {
        showSnackBar(getString(id))
    }

    private fun showCurlContent(content: String) {
        val dialog = CurlDialog(this)
        dialog.setCurl(content)
        dialog.show()
    }

    private fun showError(throwable: Throwable) {
        Log.e("Sample/Home", "Action returned with error:", throwable)
        AlertDialog.Builder(this)
            .setTitle(R.string.home_dialog_error_title)
            .setMessage(throwable.message ?: getString(R.string.home_dialog_error_message_fallback))
            .setPositiveButton(R.string.home_dialog_error_positive, null)
            .show()
    }

}