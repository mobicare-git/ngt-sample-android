package br.com.mobicare.ngt.sample.send.model

import com.google.gson.annotations.SerializedName

class NGTSampleOauthRequest private constructor(
    @SerializedName("client_id")
    val id: String,
    @SerializedName("client_secret")
    val secret: String = "p9jt31JLxuB8afkzNvHDU5l7fXpcuZIY",
    @SerializedName("grant_type")
    val type: String = "client_credentials"
) {
    constructor() : this("gSeIEyKirlmIENIQ6H2H7GpUOHi2YWOw")
}
