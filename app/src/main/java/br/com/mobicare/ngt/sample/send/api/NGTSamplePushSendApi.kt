package br.com.mobicare.ngt.sample.send.api

import android.net.Uri
import android.util.Log
import br.com.mobicare.ngt.sample.send.model.NGTSampleNotificationSend
import br.com.mobicare.ngt.sample.send.model.NGTSampleOauthRequest
import br.com.mobicare.ngt.sample.send.model.NGTSampleOauthResponse
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.TaskCompletionSource
import com.google.android.gms.tasks.Tasks
import com.google.gson.Gson
import com.moczul.ok2curl.CurlBuilder
import com.moczul.ok2curl.CurlInterceptor
import okhttp3.Call
import okhttp3.Callback
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.toRequestBody
import okhttp3.Response
import okhttp3.logging.HttpLoggingInterceptor
import java.io.IOException
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit

class NGTSamplePushSendApi(private val gson: Gson) {

    private val client: OkHttpClient
    private val url = Uri.parse("https://ngt-p.mobicare.com.br/mcare-ngt/")
    private val jsonMediaType: MediaType by lazy { "application/json".toMediaType() }
    private val executor = Executors.newSingleThreadExecutor()

    init {
        val builder = OkHttpClient.Builder()
        builder.connectTimeout(30, TimeUnit.SECONDS)
        builder.readTimeout(30, TimeUnit.SECONDS)
        builder.writeTimeout(30, TimeUnit.SECONDS)
        builder.cache(null)
        builder.addInterceptor(CurlInterceptor {
            synchronized(NGTSamplePushSendApi::class.java) {
                Log.v("Sample/Ok2Curl", it)
            }
        })
        builder.addInterceptor(HttpLoggingInterceptor {
            synchronized(NGTSamplePushSendApi::class.java) {
                Log.v("Sample/OkHttp", it)
            }
        }.apply {
            setLevel(HttpLoggingInterceptor.Level.BODY)
        })
        client = builder.build()
    }

    var oauth: NGTSampleOauthResponse? = null

    private inline fun createRequest(configure: (Request.Builder) -> Unit): Task<Response> {
        val builder = Request.Builder()
        configure(builder)
        val complete = TaskCompletionSource<Response>()
        client.newCall(builder.build()).enqueue(object : Callback {
            override fun onResponse(call: Call, response: Response) {
                complete.setResult(response)
            }

            override fun onFailure(call: Call, e: IOException) {
                complete.setException(e)
            }
        })
        return complete.task
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun getOauthToken(): Task<NGTSampleOauthResponse> = createRequest { builder ->
        builder
            .url(url.buildUpon().appendEncodedPath("v1/notification/oauth2/token").toString())
            .post(gson.toJson(NGTSampleOauthRequest()).toRequestBody(jsonMediaType))
    }.onSuccessTask(executor) {
        if (it.code != 200) {
            throw IllegalStateException(
                "Expected to receive response 200",
                RuntimeException(it.toString())
            )
        }
        val parsed = gson.fromJson(it.body!!.string(), NGTSampleOauthResponse::class.java)
        oauth = parsed
        Tasks.forResult(parsed)
    }

    @Suppress("MemberVisibilityCanBePrivate")
    fun requireOauthToken(): Task<NGTSampleOauthResponse> =
        Tasks.forResult(oauth).onSuccessTask(executor) {
            if (it != null)
                Tasks.forResult(it)
            else
                getOauthToken()
        }

    fun sendPushNotification(notification: NGTSampleNotificationSend): Task<Unit> =
        sendPushNotificationCaptureCurl(notification).onSuccessTask(executor) { Tasks.forResult(Unit) }

    fun sendPushNotificationCaptureCurl(notification: NGTSampleNotificationSend): Task<String> =
        getOauthToken().onSuccessTask {
            val temp = object {
                lateinit var request: Request
            }
            createRequest { builder ->
                builder.addHeader("Authorization", it.token)
                    .url(url.buildUpon().appendEncodedPath("v2/notification").toString())
                    .addHeader("Campaign", notification.campaign)
                    .post(gson.toJson(notification).toRequestBody(jsonMediaType))
                temp.request = builder.build()
            }.onSuccessTask(executor) {
                Tasks.forResult(Pair(temp.request, it))
            }
        }.continueWithTask(executor) {
            if (!it.isSuccessful && it.exception is IllegalStateException) {
                oauth = null
            } else if (it.isSuccessful && it.result.second.code != 200) {
                throw IllegalStateException(
                    "Expected to receive response 200",
                    RuntimeException(it.result.toString())
                )
            }
            it
        }.onSuccessTask(executor) {
            Tasks.forResult(CurlBuilder(it.first).build())
        }

}