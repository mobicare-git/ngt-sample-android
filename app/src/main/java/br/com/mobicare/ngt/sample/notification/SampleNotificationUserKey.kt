package br.com.mobicare.ngt.sample.notification

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.provider.Settings
import android.util.Base64
import br.com.mobicare.ngt.sample.BuildConfig
import java.lang.ref.WeakReference
import java.security.MessageDigest

object SampleNotificationUserKey {

    private const val KEY_USER_KEY = "KeyUserKey"

    private var preferences = WeakReference<SharedPreferences>(null)

    private fun getPreferences(context: Context): SharedPreferences {
        preferences.get()?.let { return it }
        val pref = context.applicationContext.getSharedPreferences(
            BuildConfig.APPLICATION_ID,
            Context.MODE_PRIVATE
        )
        preferences = WeakReference(pref)
        return pref
    }

    @SuppressLint("HardwareIds")
    private fun generateUserKey(context: Context): String {
        val id = Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        val encoded = Base64.encodeToString(
            MessageDigest.getInstance("SHA-256").digest(id.encodeToByteArray()),
            Base64.NO_PADDING or Base64.NO_WRAP
        )
        val userKey = "demo-" + encoded.substring(0, 8)
        saveUserKey(context, userKey)
        return userKey
    }

    fun getUserKey(context: Context): String {
        return getPreferences(context).getString(KEY_USER_KEY, null) ?: generateUserKey(context)
    }

    fun saveUserKey(context: Context, userKey: String) {
        getPreferences(context).edit().putString(KEY_USER_KEY, userKey).apply()
    }

}