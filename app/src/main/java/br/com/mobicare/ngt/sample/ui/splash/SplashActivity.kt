package br.com.mobicare.ngt.sample.ui.splash

import android.content.Intent
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import br.com.mobicare.ngt.NGT
import br.com.mobicare.ngt.sample.R
import br.com.mobicare.ngt.sample.ui.home.HomeActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        val version = packageManager.getPackageInfo(packageName, 0).versionName
        val sdk = NGT.getInstance(this).getVersion()
        val text = findViewById<TextView>(R.id.splash_version)
        text.text = getString(R.string.splash_version, sdk, version)

        text.postDelayed({
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }, 100)
    }
}