package br.com.mobicare.ngt.sample.ui.loading

import android.content.Context
import android.view.Window
import androidx.appcompat.app.AppCompatDialog
import br.com.mobicare.ngt.sample.R

class LoadingDialog(context: Context) : AppCompatDialog(context, R.style.Style_LoadingDialog) {

    init {
        supportRequestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_loading)
    }

}