# Mobi Push Notification
O que você vê em primeiro lugar quando pega seu celular? A  notificação push mais recente recebida em seu dispositivo por um de seus aplicativos. Dependendo de quão interessante é, você interage com ele ou o ignora.

Hoje, o push notification é crucial para qualquer estratégia de marketing móvel. As notificações push são mensagens de texto e atingem apenas os usuários que instalaram seu aplicativo. Eles melhoram a experiência do cliente e o envolvimento do usuário. 

Com notificações push, os desenvolvedores de aplicativos Android e iOS direcionam o tráfego e aumentam a fidelidade, fornecendo informações no lugar certo e na hora certa. 

Se você não enviar mensagens aos usuários quando eles instalarem seu aplicativo, estará desperdiçando 95% de seus gastos com aquisição. Isso porque os usuários que recebem notificações nos primeiros 90 dias têm uma taxa de retenção 180% maior do que aqueles que não recebem. (de onde vem esse dado?)
Mais de 75% dos downloads de aplicativos resultam em um único aplicativo aberto. Os usuários abrem o aplicativo uma vez e nunca mais voltam. 

Portanto, não perca esta ótima ferramenta de marketing para aumentar seu público! O Mobi Push Notification é a ferramenta ideal!

[![NPM Version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Downloads Stats][npm-downloads]][npm-url]


# O Mobi Notification Gateway fornece a melhor solução de notificações push para automatizar os seguintes casos de uso:
 
* Dê as boas-vindas aos usuários depois que eles instalarem seu aplicativo : ("Olá Antonio! Seja bem vindo, vamos conhecer o App …!")
* Informações em tempo real (“Sua solicitação acabou de ser aprovada!”)
* Envie mensagens contextuais que direcionam para uma determinada funcionalidade do seu App. (“Sua reserva começa em 15 minutos, quer fazer o seu pré-check-in agora?”)
* Direcionamento de compradores (“Venha conhecer a nova coleção de verão!”)
* Converta carrinhos abandonados (“Você está a apenas um clique de concluir sua compra!”)
* Engajar usuários inativos (“Sentimos sua falta…”)

<img src="https://www.mobicare.com.br/wp-content/uploads/2022/06/testpush01.png" width="400">
 
## Configurando o Mobi Push Notification no seu App

O Mobi Push Notification utiliza o SDK Admin do Firebase para interagir com os servidores FCM, e utilizamos o protocolo  API FCM HTTP v1, que é a opção de protocolo mais atualizada, com autorização mais segura e recursos flexíveis de mensagens entre plataformas. O SDK Admin do Firebase é baseado nesse protocolo e oferece todas as vantagens dele. Os novos recursos geralmente são adicionados apenas à API HTTP v1. Por isso, recomendamos a utilização dela API na maioria dos casos de uso.

Para isso, será necessário ter na sua conta do Firebase, uma conta de serviço ativa para utilizar o Cloud Messaging.

### O que é uma conta de serviço Firebase?

Uma conta de serviço representa uma identidade de serviço do Google Cloud, como o código que é executado nas VMs do Compute Engine, aplicativos do App Engine ou sistemas executados fora do Google. Saiba mais sobre contas de serviço.

Políticas da organização podem ser usadas para proteger contas de serviço e bloquear recursos arriscados de conta de serviço, como IAM Grants automático, criação/upload de chaves ou a criação de contas de serviço inteiramente. Saiba mais sobre políticas da organização da conta de serviço.


### O que vou precisar fazer?
Para usar o Mobi Push Notification, você precisará de:

* Passo 1: Ter um projeto do Firebase;
* Passo 2: Possuir uma  conta de serviço do SDK Admin do Firebase;
* Passo 3: Gerar um arquivo de configuração com as credenciais da sua conta de serviço;
* Passo 4: Configurar o Portal Administrativo do Mobi Push Notification;
* Passo 5: Configurar o SDK no seu App Android.

Caso você não possua um projeto no Firebase criado, siga a partir do passo 1. Caso já possua um projeto criado no Firebase, siga para o passo 2.


#### Passo 1: Criando o seu projeto Firebase

Se você ainda não tem um projeto do Firebase, crie um no Console do Firebase. Consulte Noções básicas sobre projetos do Firebase para mais informações.


No [Console do Firebase](https://console.firebase.google.com/), clique em Criar um projeto.

<img src="https://ngt.mobicare.com.br/docs/images/image11.png" width="400">
Fig. 1: Criando um novo projeto


Informe o nome do seu projeto, aceite os termos de uso do Firebase e clique em Continuar.

<img src="https://ngt.mobicare.com.br/docs/images/image7.png" width="400">
Fig. 2: Criando um novo projeto - nome do projeto


Clique novamente em Continuar.

<img src="https://ngt.mobicare.com.br/docs/images/image2.png" width="400">
Fig. 3: Criando um novo projeto - Analytics


Selecione o local do Google Analytics, aceite os termos de uso e clique em Criar projeto.

<img src="https://ngt.mobicare.com.br/docs/images/image1.png" width="400">
Fig. 4: Criando um novo projeto - conclusão



#### Passo 2: Possuir uma conta de serviço configurada

É necessário uma  conta de serviço do SDK Admin do Firebase para se comunicar com o Firebase. Ela é gerada automaticamente quando você cria um projeto do Firebase ou adiciona a plataforma a um projeto do Google Cloud.

No console do seu projeto, clique em Configurações, depois na Aba Cloud Messaging e certifique-se de que a API Firebase Cloud Messaging (V1) esteja ativada como mostra a figura 5.

<img src="https://ngt.mobicare.com.br/docs/images/image9.png" width="400">
Fig. 5: API Firebase Cloud Messaging (V1) ativada



#### Passo 3: Gerar um arquivo de configuração com as credenciais da sua conta de serviço

Uma vez tendo o seu projeto criado e o Cloud Messaging ativado, automaticamente sua conta de serviço já estará criada. Basta agora, gerar uma chave privada para que seja configurado no Portal Administrativo do Mobi Push Notification.

Na aba Contas de serviço, com a SDK Admin do Firebase selecionado, clique no botão Gerar nova chave privada.

<img src="https://ngt.mobicare.com.br/docs/images/image5.png" width="400">
Fig. 6: Chave privada do SDK Admin do Firebase


Confirme a geração da chave privada clicando no botão Gerar chave.

<img src="https://ngt.mobicare.com.br/docs/images/image8.png" width="400">
Fig. 7: Chave privada do SDK Admin do Firebase


Ao clicar em Gerar chave, será feito o download de um arquivo JSON para a área de downloads do seu computador. O arquivo possuirá um conteúdo parecido com esse da figura 8.

<img src="https://ngt.mobicare.com.br/docs/images/image6.png" width="400">
Fig. 8: JSON Chave privada

ATENÇÃO: Guarde bem esse arquivo, ele precisará ser enviado para Portal Administrativo do Mobi Push Notification para concluir a configuração da sua conta.



#### Passo 4: Configurar o Portal Administrativo do Mobi Push Notification

Precisamos agora finalizar a configuração do seu projeto para que ele possa começar a utilizar o Mobi Push Notification.

Acesse o Portal Administrativo do [Mobi Push Notification](https://ngt.mobicare.com.br/admin) e faça o seu login.

Na sua área de trabalho, vá em Configurar meu App.

Na opção Conta de Serviço Firebase, envie o arquivo JSON da chave privada gerada no passo 3 e clique em confirmar.

Tudo pronto! Seu App já está apto a começar a usar o Mobi Push Notification!



#### Passo 5: Configurar o SDK no seu App Android

##### Primeiros passos
* Obter as credenciais de acesso ao artifactory. Pegue na área Credenciais no Portal Administrativo do Mobi Push Notification.
* Obter o token de autorização para uso da api. Pegue na área Credenciais no Portal Administrativo do Mobi Push Notification.

##### Requisitos
* Target mínimo Android 5 (api 21);
* Firebase Messaging.

##### Glossário
* MOBICARE_ARTIFACTORY_NGT_USERNAME: é o nome de usuário do artifactory
* MOBICARE_ARTIFACTORY_NGT_PASSWORD: é a senha de acesso ao artifactory
* APP_AUTHORIZATION: é o token de autorização para uso da api

##### Configuração do Firebase
Para configurar o firebase e habilitar seu app para o recebimento de push notifications acesse [Configurar um app cliente do Firebase Cloud Messaging no Android](https://firebase.google.com/docs/cloud-messaging/android/client?hl=pt-br)

##### Instalação

Adicionar o nosso artifactory como repositório de dependências, usando a credencial fornecida para o cliente.

```sh
maven {
    url "https://artifactory.mobicare.com.br/artifactory/android-release-local/"
    credentials {
        username MOBICARE_ARTIFACTORY_NGT_USERNAME
        password MOBICARE_ARTIFACTORY_NGT_PASSWORD
    }
    content {
        includeModule("br.com.mobicare", "ngt")
    }
}
google()
mavenCentral()
```

Ps.: A ordem dos repositórios não importa, se manter o ```sh includeModule```

Adicionar o SDK como dependência do projeto:

```sh
implementation "br.com.mobicare:ngt:{ngt_version}"
```

Substituindo {ngt_version} pela versão mais recente.

Existem duas formas de inserir o token de autorização no projeto:

1. Pelo strings.xml
Basta adicionar no strings.xml:

```sh
<string name="ngt_sdk_authorization" translatable="false">APP_AUTHORIZATION</string>
```

2. Na inicialização do SDK
Dentro da Application, no método onCreate(), incluir:

```sh
NGT.init(this, "APP_AUTHORIZATION")
```

Observações
* Utilizar o método pelo strings.xml não requer a inicialização do SDK na Application
* Caso haja troca do token de autorização, é obrigatório usar o método de inicialização pela Application.


##### Funcionalidades

###### Register/Unregister
A interface do SDK (retornado por NGT.getInstance(context)) provê três funções para essa operação.

```sh
NGT.getInstance(context).register(fcm_token, user_key)
```

O FCM token deve ser obtido através do SDK do Firebase Messaging, enquanto que o User Key é a identificação do usuário.

```sh
NGT.getInstance(context).updateFcmToken(fcm_token)
```

Deve ser colocado dentro da classe que implementa o recebimento de push (FirebaseMessagingService), na função onNewToken().

```sh
NGT.getInstance(context).unregister()
```

Desregistra o dispositivo para recebimento de push.


###### Configurando o FirebaseMessagingService
Utilize a função NGT.getInstance(context).getNotification() para detectar que o push recebido é um push vindo do NGT. A função retorna nulo quando não é um push do NGT.

Ex.:

```sh
override fun onMessageReceived(_message: RemoteMessage) {
    val ngt = NGT.getInstance(this).getNotification(_message.data)
    if (ngt == null) {
        return
    }
```

Ao receber um push do NGT, é obrigatório registrar as PendingIntent do SDK na notificação que o desenvolvedor exibir. A classe NGTNotification já possui funções prontas para essa finalidade. Ex.:

```sh
builder.setDeleteIntent(ngt.createDeletePendingIntent(this))

builder.setContentIntent(ngt.createContentPendingIntent(this))
```

Obs.: Caso haja ação do clique na notificação, a ação desejada (que é uma PendingIntent) deve ser passada como segundo parâmetro na função createContentPendingIntent.



## Histórico de lançamentos

* 0.0.1
    * Primeira versão


## Contributing

1. Faça o _fork_ do projeto
2. Crie uma _branch_ para sua modificação
3. Faça o _commit_ 
4. _Push_ 
5. Crie um novo _Pull Request_

[npm-image]: https://img.shields.io/npm/v/datadog-metrics.svg?style=flat-square
[npm-url]: https://npmjs.org/package/datadog-metrics
[npm-downloads]: https://img.shields.io/npm/dm/datadog-metrics.svg?style=flat-square
[travis-image]: https://img.shields.io/travis/dbader/node-datadog-metrics/master.svg?style=flat-square
[travis-url]: https://travis-ci.org/dbader/node-datadog-metrics
[wiki]: https://github.com/seunome/seuprojeto/wiki